<?php 
class Feed{
	private $feed;
	private $dom;
	public function __construct(){
		$this->dom = new DOMDocument('1.0', 'UTF-8');
		$this->feed = $this->dom->appendChild(new DOMElement('rss'));
		$this->feed->setAttribute('version', '2.0');
		$this->feed->setAttribute('xmlns:atom', 'http://www.w3.org/2005/Atom');
		return $this->feed;
	}
	public function setAttribute($name, $value, $node=null){
		if($node){
			$node->setAttributeNode(new DOMAttr($name, $value));
		}else{
			$this->feed->setAttributeNode(new DOMAttr($name, $value));
		}
	}

	public function setCData(DOMNode &$node, $value){
		$node->appendChild(new DOMCdataSection($value));
	}
	public function createNode($newNode, $node = null){
		if($node){
			return $node->appendChild($this->dom->createElement($newNode));
		}else{
			return $this->feed->appendChild($this->dom->createElement($newNode));
		}
	}
	public function setText($node, $text){
		$node->appendChild($this->dom->createTextNode($text));
	}
	public function save(){
		return $this->dom->saveXML();
	}
	public function save2file($filename){
		$this->dom->save($filename);
	}
}
?>