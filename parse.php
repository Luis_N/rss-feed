<?php
class Parse{
	private $data = array();
	private $date;
	private $src;
	private $sdb;
	public $feed;
	private $reCache = false;
	private $force = false;
	private function setSource($date = null, $section = "Portada"){
		$base_url = "http://www.eltelegrafo.com/index.php";
		$sec_url = "&seccion=";
		$date_url = "?fechaedicion=";
		if( !is_null($date) && ( $date !== date('d-m-Y') ) ){
			$edate = preg_replace("/([0-9]{2})-([0-9]{2})-([0-9]{4})/", "$3-$2-$1", $date);
			$base_url .= $date_url . $edate;
		}
		$sections = array('opinion','locales','nacionales','policiales','funebres','rurales','deportes');
		if(in_array($section, $sections)){
			$base_url .=  $sec_url . $sections[$section];
		}
		return $base_url;
			
	}
	protected function getSource($url){
			if(!$this->ch){
				$this->ch = curl_init();
			}
			curl_setopt($this->ch, CURLOPT_URL, $url);
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->ch, CURLOPT_REFERER, 'http://www.eltelegrafo.com');
			curl_setopt($this->ch, CURLOPT_USERAGENT, 'Dalvik/1.6.0 (Linux; U; Android 4.0.4; A13-MID Build/IMM76D)');
			curl_setopt($this->ch, CURLOPT_COOKIESESSION, true);
			curl_setopt($this->ch, CURLOPT_TIMEOUT, 60);
			$this->html = curl_exec($this->ch);
			curl_setopt($this->ch, CURLINFO_HEADER_OUT, true);
			file_put_contents("Header.txt", curl_getinfo($this->ch,CURLINFO_EFFECTIVE_URL)."\r\n",FILE_APPEND);
			if(curl_errno($this->ch) <> 0){
				curl_close($this->ch);
				throw new Exception('Error al obtener fuente de datos' . curl_error($this->ch), E_USER_ERROR);
			}
	}
	public function getDate($is8288){
		return ($is8288) ? date('r', strtotime($this->date . " " . date('H:i:s'))) : $this->date;
	}
	public function elTelegrafoDate(){
		return preg_replace("/([0-9]{2})-([0-9]{2})-([0-9]{4})/", "$3-$2-$1", $this->date);
	}
	public function saveRegister(){
		if(empty($this->data))
			return false;
		$serialize = json_encode($this->getData());
		$fp = fopen($this->sdb, 'w+');
		fwrite($fp, $serialize);
		fclose($fp);
		return true;
	}
	public function readCache(){
		$filename = $this->sdb;
		$fi = fopen($filename, 'r');
		$cache = fread($fi, filesize($filename));
		fclose($fi);
		return json_decode($cache,true);
	}
	private function initTime(){
		$f_array = explode("-",$this->date);
		$this->sdb = "sdb/{$f_array['2']}/{$f_array['1']}/eltelegrafo-{$this->date}.json";
		$this->feed = "xml/{$f_array['2']}/{$f_array['1']}/eltelegrafo-{$this->date}.xml";
	}
	public function __construct(){
		$this->date = date('d-m-Y');
		$this->initTime();
	}
	/*
		Obtiene los datos analizados
		@return Array
	*/
	public function getData(){
		return $this->data;	
	}
	/*
		Convierte la fecha de fromato cadena a date YYYY/MM/DD H:i:s
		@return String
	*/
	private function fecha2time($fecha){
		$meses = array('Enero'=>01,'Febrero'=>02,'Marzo'=>03,'Abril'=>04,'Mayo'=>05,'Junio'=>06,'Julio'=>07,'Agosto'=>08,'Septiembre'=>09,'Octubre'=>10,'Noviembre'=>11,'Diciembre'=>12);
		preg_match("/Paysandú, (Lunes|Martes|Miércoles|Jueves|Viernes|Sábado|Domingo) ([0-9]{2}) de (Enero|Febrero|Marzo|Abril|Mayo|Junio|Julio|Agosto|Septiembre|Octubre|Noviembre|Diciembre) de ([0-9]{4})/", $fecha, $output_array);
		return $output_array[4] . '-' .$meses[$output_array[3]] . '-' . $output_array[2] . ' 2:30:00';
	}
	
	/*
		Retorna true si se necesita actualizar
		@param int
		@return boolean
	*/
	private function checkTimestamp($timestamp){
		$filename = 'timestamp';
		if(!file_exists($filename)){
				touch($filename);
				return true;
			}else{
			$fr = fopen($filename, 'r');
			$oldTimestamp = fread($fr, filesize($filename));
			fclose($fr);
			if($oldTimestamp < $timestamp){
				return true;
			}
				
		}
		return false;
	}
	/*
		Actualiza la marca de tiempo
		@param int
		@return null
	*/
	private function updateTimestamp($timestamp){
		$filename = 'timestamp';
		$fr = fopen($filename, 'w+');
		fwrite($fr, $timestamp);
		fclose($fr);
	}
	/*
		Inicia el parser XML
		@return DOMXpath
	*/
	private function initialize(){
		$dom = new DOMDocument();
		if(!$dom->loadHTML($this->html)){
			var_dump($this->html);
			throw new Exception('Error al procesar la fuente de datos: "' . $this->src .'"', E_USER_ERROR);
		}
			
		$xpath = new DOMXPath($dom);
		$xpath->registerNamespace('html','http://www.w3.org/1999/xhtml');
		return $xpath;
	}
	/*
		Obtiene la fecha desde el archivo fuente
	*/
	private function  getFecha(){
		$xpath = $this->initialize();
		$fch = $xpath->query("//div[@id='fecha']");
		foreach ($fch->item(0)->childNodes as $fchchilds) {
			if($fchchilds->nodeName == 'p'){
				$fecha = trim(htmlspecialchars($fchchilds->nodeValue));
				break;
			}
		}
		if(!$fecha){
			//throw new Exception('Formato de origen desconocido', E_USER_ERROR);
			$fecha = 'Paysandú, Lunes 15 de Octubre de 1990';
		}
		return $fecha;
	}
	/*
		Analiza el codigo html para extraer los datos
	*/
	private function parseHTML(){
		$i=0;
		$xpath = $this->initialize();
		$f = $xpath->query("//div[@id='features']");
		foreach ($f->item(0)->childNodes as $childs) {
			foreach ($childs->childNodes as $value) {
				if($value->nodeName == 'p'){
					$this->data[$i]['body_short'] = trim(htmlspecialchars($value->nodeValue));
				}
				if($value->nodeName == 'div'){
					foreach ($value->childNodes as $v) {
						if($v->nodeName == 'a'){
							$this->data[$i]['link'] =  'http://www.eltelegrafo.com/' . $v->getAttribute('href');
						}
					}
				}
			}
			$i++;
		}
		$c_n = $xpath->query("//div[@class='contenido_noticia']");
		foreach ($c_n as $childs) {
			foreach ($childs->childNodes as $value) {
				if($value->nodeName == 'p'){
					$this->data[$i]['body_short'] = trim(htmlspecialchars($value->nodeValue));
				}
				if($value->nodeName == 'div'){
					foreach ($value->childNodes as $v) {
						if($v->nodeName == 'a'){
							$this->data[$i]['link'] =  'http://www.eltelegrafo.com/' . $v->getAttribute('href');
						}
					}		
				}
			}
			$i++;
		}
	}
	public function getArticle($url){
		$d = date("Y-m-d",strtotime($this->date));
		$this->getSource($url);
		$xpath = $this->initialize();
		$contenido_noticia = $xpath->query("//div[@class='contenido_noticia']")->item(0);
		$title = $xpath->query("h2", $contenido_noticia)->item(0);
		$articulo['title'] = htmlspecialchars($title->nodeValue);
		$noticia = $xpath->query("div[@class='noticia']", $contenido_noticia)->item(0);
		$foto = @$xpath->query("div[@class='foto']",$noticia)->item(0);
		if($foto){
			$articulo['img'] = 'http://www.eltelegrafo.com/' . $foto->childNodes->item(0)->getAttribute('src');
		}
		$p =  $xpath->query("p", $noticia)->item(0);
		preg_match("/([\w]*)  \| ([\d]*) ([\w]*)/i", $p->textContent,$match);
		//
		$articulo['section'] =  trim($match[1]);
		$articulo['body'] = trim(preg_replace("/([\w]*)  \| ([\d]*) ([\w]*)/i", "", $p->textContent));
		$articulo['date'] = $this->date;
		return $articulo;
	}
	/*
		Genera el feed en cuestion, ya sea leyendolo desde la cache
		o generarndolo propiamente dicho
		@return string
	*/
	public function generate(){
		//Si el archivo fuente no existe se descarga
		$this->getSource("www.eltelegrafo.com/");
		$timestamp = $this->checkTimestamp(strtotime($this->fecha2time($this->getFecha())));
		if($timestamp){
			/* Si la marca de tiempo es obsoleta se analiza el HTML fuente */
			$this->parseHTML();
			if(!empty($this->data)){
				foreach($this->data as $data){
					$as[] = array_merge($this->getArticle($data['link']),$data);
				}
				$this->data = $as;
				$feed = $this->saveFeed();
			}
			
			/* Si el registro se guarda apropiadamente se actualiza la marca de tiempo */
			if($this->saveRegister()){
				$this->updateTimestamp(strtotime(date('Y-m-d H:i:s', strtotime($this->fecha2time($this->getFecha())) + (1 * 24 * 60 * 60))));
			}
		}else{
			/* Si la marca de tiempo aun esta vigente siemplemente leenmos la cache del feed */
			//$this->data = $this->readCache();
			if(!file_exists($this->feed)){
				$this->setDate(date('d-m-Y',strtotime($this->date) - (1 * 24 * 60 * 60)));
				$this->generate();
				
			}
			$feed = $this->readFeed();
		}
		return $feed;
	}
	/*
		Generá, almacena y retorna el feed en formato xml
		@return string
	*/
	public function saveFeed(){
		$f = new Feed();
		$channel = $f->createNode('channel');
		$title = $f->createNode('title', $channel);
		$f->setText($title,'Diario El Telegrafo');
		$link = $f->createNode('link', $channel);
		$f->setText($link,'http://www.eltelegrafo.com');
		$description = $f->createNode('description', $channel);
		$f->setText($description, 'El Telegrafo pagina principal');
		$lastBuildDate = $f->createNode('lastBuildDate', $channel);
		$f->setText($lastBuildDate, $this->getDate(true));
		$atom = $f->createNode('atom:link', $channel);
		$f->setAttribute("href", "http://encom.uy/eltelegrafo/rss.php", $atom);
		$f->setAttribute("rel", "self", $atom); 
		$f->setAttribute("type", "application/rss+xml", $atom);
		$notices = $this->getData();
  		foreach ($notices as $n) {
    		$item = $f->createNode('item' , $channel);
    		$title = $f->createNode('title' , $item);
    		$f->setText($title,$n['title']);
    		$link = $f->createNode('link' , $item);
    		if($n['img']){
    			$n['body'] = "<img src='http://www.encom.uy/eltelegrafo/ipro.php?img=" . base64_encode($n['img']) ."' /><p>{$n['body']}</p>";
    		}
    		$f->setText($link ,$n['link']);
   			$guid = $f->createNode('guid' , $item);
    		$f->setText($guid,$n['link']);
    		$description = $f->createNode('description' , $item);
    		$f->setText($description,$n['body']);
    		$pubDate = $f->createNode('pubDate' , $item);
   			$f->setText($pubDate ,$this->getDate(true));
   			$category = $f->createNode('category' , $item);
    		$f->setText($category,$n['section']);	
		}	
		$f->save2file($this->feed);
	  	return $f->save();
	}
	/*
		Lee la marca de tiempo
		@return int
	*/
	public function readTimestamp(){
		$filename = 'timestamp';
		$fr = fopen($filename, 'r');
		$buffer = fread($fr, filesize($filename));
		fclose($fr);
	}
	/*
		Lee el feed desde la cache
		@return string
	*/
	public function readFeed(){
		$fp = fopen($this->feed,'r');
  		$data = fread($fp, filesize($this->feed));
  		fclose($fp);
  		return $data;
	}
}