<?php
ini_set("display_errors","on");
ini_set('max_execution_time',0);
error_reporting(~E_WARNING & ~E_NOTICE);
date_default_timezone_set('America/Montevideo');
require('feed.php');
require('parse.php');
$p = new Parse();
header('Content-Type: text/xml; charset=UTF-8;');
header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($p->feed)).' GMT', true, 200);
if(!empty($_GET['fecha'])){
  $p->setDate($_GET['fecha']);
}



echo $p->generate();

?>